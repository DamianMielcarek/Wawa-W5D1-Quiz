package com.akademiakodu.quizweekend;


import java.util.List;

public interface IQuestionsDatabase {
    List<Question> getQuestions();
}
