package com.akademiakodu.quizweekend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartGameActivity extends AppCompatActivity {
    @BindView(R.id.name_field)
    EditText mEditText;

    private IQuestionsDatabase mQuestionsDatabase = new RandomQuestionsDatabase();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.next_button)
    void openNextScreen() {
        // 1. Odczytac wpisany tekst
        String name = mEditText.getText().toString();
        // 2. Otworzyc nowe okno przekazujac wpisany tekst
        Intent nameIntent = new Intent(this, GreetingActivity.class);
        nameIntent.putExtra(GreetingActivity.EXTRA_NAME, name);

        // Losowanie pytań
        List<Question> questions = mQuestionsDatabase.getQuestions();
        Random random = new Random();
        while (questions.size() > 5) {
            // Usuwa losowy element z listy
            // (random.nextInt(questions.size()) usuwa losowy eement na liście)
            questions.remove(random.nextInt(questions.size()));
        }
        Collections.shuffle(questions);

        nameIntent.putExtra(GreetingActivity.EXTRA_QUESTIONS, new ArrayList<>(questions));

        startActivity(nameIntent);
    }
}













